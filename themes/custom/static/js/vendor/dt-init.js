// jQuery Datatables plugin initializer

/*

// single table

$(document).ready( function () {
    $('#myTable').DataTable();
} );


// multiple tables (in theory)
// https://datatables.net/examples/basic_init/multiple_tables.html
// does not seem to work

$(document).ready(function() {
    $('table.display').DataTable();
} );

*/

$(document).ready(function() {

    $('#dt').DataTable( {
    // $('table.stripe').DataTable( {

        "language": {
            "search": "Filter rows: "
        },

        "order": [[ 2, "asc" ]],
       
    } );

} );
