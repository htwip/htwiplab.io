+++
#-------------------------------------------------------------------------------
# this is the TOML front matter format
# https://gist.github.com/oconnor663/9aeb4ed56394cb013a20
# https://gohugo.io/content-management/front-matter/
# https://gohugo.io/content-management/archetypes/
# https://gohugo.io/content-management/types/
# https://discourse.gohugo.io/t/using-site-variables-in-front-matter/7928
#
#-------------------------------------------------------------------------------

# in post list page using Datatables, cells with this default weight are hidden,
# and the "pinned" attribute is not set in the post single metadata template
weight = 99999

toc = false # how are second-level links and deeper handled, if at all?
draft = true

# author = "{{ .Site.Author.default.name }}"

# `type` must be used with `layout` if you want it to use the desired template
# type = "section"
# layout = "archives"


# series = [] # can be empty?
categories = [ "_Default" ]
tags = [
    "_tag1",
    "_tag2" 
    ]
# format = [ "_Default" ] # e.g., "Normal Post", "Links List", "Video Playlist"
# group = [ "_Default" ] # e.g., "Demo"

# aliases = [ "old-file-name-1" ] # uncomment if you rename anything already crawled
# description = "Article search engine description (also for <meta> tag)."
title = "{{ replace .Name "-" " " | title }}" # Title of the blog post.

# for timestamps, use direnv to export TZ="My/Preferred_Zone" instead of git
# expiryDate = {{ .Date }} # probably will never use this
date = {{ .Date }} # timestamp of post creation, for documentation only, not used in templates
lastmod = {{ .Date }} # set this to override Hugo's own automatically generated lastmod timestamp
publishDate = {{ .Date }} # this is your basic date used in templates

#-------------------------------------------------------------------------------
+++

If you put anything here prior to a manual summary splitter (in lieu summary splitting via config), <!--more--> it should differ from the "description" front matter above.

**Lorem ipsum dolor sit amet**, consectetur adipiscing elit. Proin egestas dolor commodo, sollicitudin lorem semper, tristique elit.

### Do Not Use H1 or H2 In Posts

H1 is already reserved in templates for the site title, and H2 is already reserved for the post title (set in the front matter above).
