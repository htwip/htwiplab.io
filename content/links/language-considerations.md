+++
publishDate = 2021-07-05T14:08:12Z
title = "Language Considerations"
weight = 2 # 999 is rendered with span class transparent-sortable-text
categories = [ "Communication" ]
tags = [ "Commerce" , "Multi Section Tag" ]
+++

## Via Misc Sources
- [Misidentification of Alphanumeric Symbols](https://www.ismp.org/resources/misidentification-alphanumeric-symbols)
- [The Case Against Intelligent Part Numbers](https://blog.trimech.com/the-case-against-intelligent-part-numbers)
    - [Intelligent part numbers: The cost of being too smart](http://www.buyplm.com/plm-good-practice/intelligent-part-number-scheme.aspx)
    - [Part numbering system design](http://www.buyplm.com/plm-good-practice/part-numbering-system-software.aspx)
- [Your ability to read this message reveals something incredible about the mind](https://www.businessinsider.com.au/can-you-read-this-message-2016-2)
- [The difference between dyslexia and dyscalculia ](https://www.understood.org/articles/en/the-difference-between-dyslexia-and-dyscalculia)

## Via Wikipedia
- [Constructed language](https://en.wikipedia.org/wiki/Constructed_language)
- [Esperanto](https://en.wikipedia.org/wiki/Esperanto)
- [Idioglossia](https://en.wikipedia.org/wiki/Idioglossia)
- [Interglossa](https://en.wikipedia.org/wiki/Interglossa)
- [Non-lexical vocables in music](https://en.wikipedia.org/wiki/Non-lexical_vocables_in_music)
- [Written language](https://en.wikipedia.org/wiki/Written_language)
- [Linguistics](https://en.wikipedia.org/wiki/Linguistics)
    - [Formal language](https://en.wikipedia.org/wiki/Formal_language)
    - [Phoneme](https://en.wikipedia.org/wiki/Phoneme)
    - [Writing system](https://en.wikipedia.org/wiki/Writing_system)
        - [List of Latin-script alphabets](https://en.wikipedia.org/wiki/List_of_Latin-script_alphabets)
        - [Diacritic](https://en.wikipedia.org/wiki/Diacritic)
        - [Orthography](https://en.wikipedia.org/wiki/Orthography)
            - [Multigraph (orthography)](https://en.wikipedia.org/wiki/Multigraph_(orthography))
        - [Word divider](https://en.wikipedia.org/wiki/Word_divider)
        - [Unicode](https://en.wikipedia.org/wiki/Unicode)
        - [Character (computing)](https://en.wikipedia.org/wiki/Character_(computing))
        - [Grapheme](https://en.wikipedia.org/wiki/Grapheme)
        - [Ligature (writing)](https://en.wikipedia.org/wiki/Ligature_(writing))
        - [Allography](https://en.wikipedia.org/wiki/Allography)
        - [Letter case](https://en.wikipedia.org/wiki/Letter_case)
- [Keyboard layout](https://en.wikipedia.org/wiki/Keyboard_layout)
    - [Dvorak keyboard layout](https://en.wikipedia.org/wiki/Dvorak_keyboard_layout)
- [Numeronym](https://en.wikipedia.org/wiki/Numeronym)

## Tangents
- [I've got a question about words. In our subconscious do we have words that are hybrids of many words fused into one?](https://twitter.com/IntuitMachine/status/1415320403032092672)
