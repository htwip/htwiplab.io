+++
publishDate = 2021-07-14T17:29:14Z
title = "Obsolescence In Food and Beverage"
weight = 999 # 999 is rendered with span class transparent-sortable-text
# categories = [ "", "" ]
# tags = [ "", "" ]
+++

## Tangents

### COVID-Related (General)
- [The Year That Changed Restaurants—and What Happens Next](https://www.bonappetit.com/story/2020-year-that-changed-restaurants)

### COVID-Related Customer Entitlment
- [The Customer Is Not Always Right ](https://www.foodandwine.com/fwpro/customer-is-not-always-right)
- [Restaurant Shuts Down for a ‘Day of Kindness’ After Customers Make Its Staff Cry](https://www.nytimes.com/2021/07/14/us/apt-cape-cod-restaurant-workers-covid.html)
- [Brickley’s closes Wakefield location for the season due to rude customers ](https://www.wpri.com/business-news/brickleys-closes-wakefield-location-for-the-season-due-to-rude-customers/)