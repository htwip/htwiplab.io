+++
publishDate = 2021-07-05T18:17:09Z
title = "Ground Effect Vehicle Evolution"
weight = 999 # 999 is rendered with span class transparent-sortable-text
categories = [ "Transport" ]
tags = [ "Commerce", "Electric Vehicles", "Environment" ]
+++

## Via Misc Sources
- ["Hovering" boats could solve one of the biggest problems for **electric** aircraft](https://www.inverse.com/innovation/solar-powered-flying-boat) (emphasis added)
- [Regent to build high-speed **electric** ground-effect "seagliders"](https://newatlas.com/aircraft/regent-seaglider-ground-effect/) (emphasis added)
- [Why aren't we using ekranoplans?](https://www.dedoimedo.com/physics/ekranoplans.html)
- [Dark Roasted Blend: Ekranoplans Showcase, Part 2](https://www.darkroastedblend.com/2009/06/ekranoplans-showcase-part-2.html)

## Via Wikipedia
- [Ground-effect vehicle](https://en.wikipedia.org/wiki/Ground-effect_vehicle)
    - [*Lun*-class ekranoplan](https://en.wikipedia.org/wiki/Lun-class_ekranoplan)
- [Lifting body](https://en.wikipedia.org/wiki/Lifting_body)
- [Aurora D8](https://en.wikipedia.org/wiki/Aurora_D8)
    -[Boundary layer ingestion](https://en.wikipedia.org/wiki/Boundary_layer#Boundary_layer_ingestion)
- [Blended wing body](https://en.wikipedia.org/wiki/Blended_wing_body)
    - [Liberdade class underwater glider](https://en.wikipedia.org/wiki/Liberdade_class_underwater_glider)
- [Flying boat](https://en.wikipedia.org/wiki/Flying_boat)
- [Ducted fan](https://en.wikipedia.org/wiki/Ducted_fan)
    - [Contra-rotating propellers](https://en.wikipedia.org/wiki/Contra-rotating_propellers)
    - [Counter-rotating propellers](https://en.wikipedia.org/wiki/Counter-rotating_propellers)
    - [Pusher configuration](https://en.wikipedia.org/wiki/Pusher_configuration)
    - [Push-pull configuration](https://en.wikipedia.org/wiki/Push-pull_configuration)
        - [Dornier Seastar](https://en.wikipedia.org/wiki/Dornier_Seastar)
    - [Propfan](https://en.wikipedia.org/wiki/Propfan)
- [Sailing hydrofoil](https://en.wikipedia.org/wiki/Sailing_hydrofoil)
- [Hydroplane (boat)](https://en.wikipedia.org/wiki/Hydroplane_(boat))


## Via YouTube
- [Worlds First Autonomous Ground Effect Vehicle/Ekranoplan? RCTestFlight Collaboration](https://www.youtube.com/watch?v=_Cq0ffFiJUY)
- [The Flying Ship Company Concept (Full Length)](https://www.youtube.com/watch?v=_tuEsvLlYdc)
- [Bell X-22, V/STOL X-plane with four tilting ducted fans - test flight video](https://www.youtube.com/watch?v=lFdV5CVXGGw)
- [Aero-TV: Walter Mitty's Near-Jet - The PJ-II Dreamer Ducted Fan](https://www.youtube.com/watch?v=OkyUr-3ZtQ8)
- [Caspian Sea Monster Ekranoplan Flight Video](https://www.youtube.com/watch?v=V8Nu94khHoo)

## Extra
- [Bartini Beriev VVA-14](https://en.wikipedia.org/wiki/Bartini_Beriev_VVA-14)
    - [MVA-62 [*translated*]](https://translate.google.com/translate?hl=en&sl=es&u=https://www.ecured.cu/Bartini_MVA-62&prev=search&pto=aue)
    - [The Strangest Aircraft Ever Built: The Soviet Union's VVA-14](https://www.youtube.com/watch?v=UD7xiWWs-bs)
    - [Beriev Be-2500 [*and related links*]](https://en.wikipedia.org/wiki/Beriev_Be-2500)
