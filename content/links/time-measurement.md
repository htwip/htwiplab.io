+++
publishDate = 2021-07-05T16:03:54Z
title = "Time Measurement" # Title of the blog post.
weight = 999 # 999 is rendered with span class transparent-sortable-text
# categories = [ "", "" ]
tags = [ "Commerce" ]
+++

## Via Misc Sources
- [A radical calendar would eliminate time zones and leap years](https://www.businessinsider.com/a-radical-calendar-would-eliminate-time-zones-and-leap-years-2016-2)
- [Is It Time to Overhaul the Calendar?](https://www.scientificamerican.com/article/is-it-time-to-overhaul/)
- [Calendar Reform](https://www.freexenon.com/social-activism/calendar-reform/)
- [Alternative and Proposed Calendars](https://calendars.wikia.org/wiki/Alternative_and_Proposed_Calendars)
- [Why Restaurants Should Use 13 Period Accounting Calendar / Why Restaurants Should Ditch Their Monthly Financials](http://www.805accounting.com/monthly-financials/)
- [Can We Fix Daylight-Saving Time for Good?](https://www.newyorker.com/tech/annals-of-technology/can-we-fix-daylight-saving-time-for-good)
- [Proof daylight saving time is dumb, dangerous, and costly](https://www.bostonglobe.com/news/nation/2017/03/10/proof-daylight-saving-time-dumb-dangerous-and-costly/kOqQs7T33rYHMEnCraQSJO/story.html)

## Via Wikipedia
- [Chronometry](https://en.wikipedia.org/wiki/Chronometry)
- [Perpetual calendar](https://en.wikipedia.org/wiki/Perpetual_calendar)
- [Perennial calendar](https://en.wikipedia.org/wiki/Perennial_calendar)
- [Lunisolar calendar](https://en.wikipedia.org/wiki/Lunisolar_calendar)
- [Category:Leap week calendars](https://en.wikipedia.org/wiki/Category:Leap_week_calendars)
- [Calendar reform](https://en.wikipedia.org/wiki/Calendar_reform)
    - [International Fixed Calendar](https://en.wikipedia.org/wiki/International_Fixed_Calendar)
    - [World Calendar](https://en.wikipedia.org/wiki/World_Calendar)
    - [Pax Calendar](https://en.wikipedia.org/wiki/Pax_Calendar)
- [Category:Specific calendars](https://en.wikipedia.org/wiki/Category:Specific_calendars)
- [Baháʼí calendar](https://en.wikipedia.org/wiki/Bah%C3%A1%CA%BC%C3%AD_calendar)
- [Irish calendar](https://en.wikipedia.org/wiki/Irish_calendar)