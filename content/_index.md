+++
layout = "static-page"

# note: if you leave the leading underscore off of this file name,
# the rest of the Content directory folders will yield 404 not
# found errors
+++

This page intentionally left blank for now.

In the meantime, select a menu entry from above for actual content.
